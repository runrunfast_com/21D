# 前端开发规范

## 目的

为提高团队协作效率，便于项目后期维护、优化。本文档如有不对或者不合适的地方请及时提出，经讨论决定后方可更改。

## 原则

1. 符合web标准，
2. 语义化。
3. 结构、表现、行为三者相互分离。
4. 页面性能方面优化。

## 内容

规范要求如下

> [HTML 规范](./html.md)  
> [图片规范](./img.md)  
> [CSS 规范](./css.md)  
> [JavaScript 规范](./js.md)

## 参考资料

> [TGideas团队WEB规范](http://tguide.qq.com/main/)  
> [关于团队合作的css命名规范-腾讯AlloyTeam前端团队](http://www.alloyteam.com/2011/10/css-on-team-naming/)  
> [百度前端开发规范-fex-team](https://github.com/fex-team/styleguide)  
> [Airbnb JavaScript 编码规范](https://github.com/yuche/javascript#table-of-contents/)  
> [前端开发规范手册](https://github.com/Aaaaaashu/Guide/)
> [FECS前端代码风格工具套件](https://github.com/Aaaaaashu/Guide/)
