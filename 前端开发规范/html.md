# HTML 规范

* [强制]HTML文件必须加上 DOCTYPE 声明，并统一使用 HTML5 的文档声明

    ```html
    <!DOCTYPE html>
    ```

* [建议] 在 `html` 标签上设置正确的 `lang` 属性。  
  推荐使用属性值 `cmn-Hans-CN`（简体, 中国大陆），但是考虑浏览器和操作系统的兼容性，目前仍然使用 `zh-CN` 属性值

    ```html
    <!-- 中文页面 -->
    <html lang="zh-CN">

    <!-- 英文页面 -->
    <html lang="en">
    ```  

    更多地区语言参考：
    >标准 | 建议
    >-|-|
    >`zh-SG` 中文 (简体, 新加坡)| `cmn-Hans-SG` 普通话 (简体, 新加坡)  
    >`zh-HK` 中文 (繁体, 香港)| `cmn-Hant-HK` 普通话 (繁体, 香港)  
    >`zh-MO` 中文 (繁体, 澳门)| `cmn-Hant-MO` 普通话 (繁体, 澳门)  
    >`zh-TW` 中文 (繁体, 台湾)| `cmn-Hant-TW` 普通话 (繁体, 台湾)  
    以下写法已于 2009 年废弃，不推荐使用
    >zh-Hans, zh-Hans-CN, zh-Hans-SG, zh-Hans-HK, zh-Hans-MO, zh-Hans-TW,  
    >zh-Hant, zh-Hant-CN, zh-Hant-SG, zh-Hant-HK, zh-Hant-MO, zh-Hant-TW

* [强制] HTML标签名、类名、标签属性和大部分属性值统一用小写。引号一律使用双引号。  
  HTML文本、CDATA、JavaScript、meta标签某些属性等内容可大小写混合

* [强制] 页面必须包含 `title` 标签声明标题。

* [强制] class 必须单词全字母小写，严禁使用驼峰命名，单词间以 - 分隔。

* [强制]在 HTML 中不能使用小于号 “<” 和大于号 “>”特殊字符，浏览器会将它们作为标签解析，若要正确显示，在 HTML 源代码中使用字符实体

    ```html
    <!-- good -->
    <a href="#">more&gt;&gt;</a>

    <!-- bad -->
    <a href="#">more>></a>
    ```

* [强制] class 必须代表相应模块或部件的内容或功能，不得以样式信息进行命名。

    ```html
    <!-- good -->
    <div class="sidebar"></div>

    <!-- bad -->
    <div class="left"></div>
    ```

* [强制] 元素 `id` 页面有且仅有一个。

* [建议] 元素 `id` 建议使用驼峰命名，同项目必须保持风格一致。

* [强制] 标签使用必须符合web标签嵌套规则。  
  例如 `div` 不得置于 `p` 中，`tbody` 必须置于 `table` 中，`li`必须置于`ul`中。

* [建议] DOM结构应在保证弹性的基础上尽量减少嵌套层数。减少不必要的标签。  
 较少的DOM节点有利于页面的渲染以及选择器的消耗。

    ```html
    <!-- good -->
    <img class="avatar" src="image.png">

    <!-- bad -->
    <span class="avatar">
        <img src="image.png">
    </span>
    ```

* [强制] 有文本标题的控件必须使用 `label` 标签将其与其标题相关联。  
  有两种方式：
  1. 将控件置于 `label` 内。
  2. `label` 的 `for` 属性指向控件的 `id`。

  推荐使用第一种，减少不必要的 `id`。如果 DOM 结构不允许直接嵌套，则应使用第二种。

    ```html
    <label><input type="text" name="username" value=""> 用户名：</label>

    <label for="username">用户名：</label>
    <input type="text" name="username" id="username" value="">
    ```

* [强制]引入JS库文件，文件名须包含库名称及版本号及是否为压缩版，比如jquery-1.10.1.min.js；  
 引入插件，文件名格式为库名称+插件名称，比如jQuery.scroll.js。
