# JavaScript 规范

* 对于空格、换行、行尾分号、逗号、括号、缩进等代码风格的书写要求。统一使用 jshint 、或ESLint的配置检查。

* [强制]在使用ES6模块语法声明变量时,若使用`let`、`const`关键字，后续中不应使用`var`。

* [强制]使用字面量值创建对象、数值

    ```javascript
    // good
    const a = {};
    const items = [];

    // bad
    const a = new Object{};
    const items = new Array();
    ```

* [强制]文档注释，以下情况都需给出注释
  * 难于理解的代码段
  * 可能存在错误的代码段
  * 浏览器特殊的HACK代码
  * 业务逻辑强相关的代码
  * 全局使用的参数
  * 所有函数、类的用途，以及函数中重要的参数

  ```javascript
  /**
   * @func
   * @desc 一个带参数的函数
   * @param {string} a - 参数a
   * @param {number} b=1 - 参数b默认值为1
   * @param {string} c=1 - 参数c有两种支持的取值</br>1—表示x</br>2—表示xx
   * @param {object} d - 参数d为一个对象
   * @param {string} d.e - 参数d的e属性
   * @param {string} d.f - 参数d的f属性
   * @param {object[]} g - 参数g为一个对象数组
   * @param {string} g.h - 参数g数组中一项的h属性
   * @param {string} g.i - 参数g数组中一项的i属性
   * @param {string} [j] - 参数j是一个可选参数
   */
  function foo(a, b, c, d, g, j) {
    /* ... */
  }
  ```

* [强制]永远不要直接使用undefined进行变量判断；使用typeof和字符串'undefined'对变量进行判断。

    ```javascript
    // good
    if (typeof person === 'undefined') {
        /* ... */
    }

    // bad
    if (person === undefined) {
        /* ... */
    }
    ```

* [强制]禁止声明了变量却不使用；

* [强制]禁止`debugger`、`console.log()`出现在提交的代码里；

* [强制]对上下文this的引用只能使用 `_this`、`that`、 `self`其中一个来命名；建议使用`self`

* [强制] 使用单独的变量保存需要多次使用的 jQuery 对象，以减少多余的元素查询，提高性能。

```javascript
// good
var $inputUserName = $('input[name="username"]');
$inputUserName.val();
$inputUserName.parent().removeClass('has-error');

// bad
$('input[name="username"]').val();
$('input[name="username"]').parent().removeClass('has-error');
```
