# CSS 规范

* [强制]样式文件必须写上 @charset 规则，并且一定要在样式文件的第一行首个字符位置开始写，编码名用 “UTF-8”

    ```css
    @charset "UTF-8";
    /**
    * @desc File Info
    * @author Author Name
    * @date 2015-10-10
    */

    .jdc {
        /* ... */
    }
    ```

* [强制]不允许有空的规则。

* [建议]不使用 ID 选择器。

* [强制]属性值'0'后面不要加单位。

* [强制]每个属性声明末尾都要加分号。

* [建议]不使用无具体语义定义的元素选择器。

* [建议]选择器的嵌套层级应不大于 3 级，位置靠后的限定条件应尽可能精确

* [强制]CSS3 浏览器私有前缀在前，标准前缀在后。

    ```css
    .jdc {
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        -o-border-radius: 10px;
        -ms-border-radius: 10px;
        border-radius: 10px;
    }
    ```
* [建议]媒体查询。  
    尽量将媒体查询的规则靠近与他们相关的规则，不要将他们一起放到一个独立的样式文件中，或者丢在文档的最底部，这样做只会让大家以后更容易忘记他们。

    ```css
    .element {
       /* ... */
    }

    .element-avatar{
        /* ... */
    }

    @media (min-width: 480px) {
        .element {
            /* ... */
        }

        .element-avatar {
            /* ... */
        }
    ```

* [建议]ClassName 的命名。  
    ClassName 的命名应该尽量精短、明确，必须以字母开头命名，且全部字母为小写，单词之间统一使用中横线 “\-” 连接，对于组件命名建议采用姓氏命名法（继承 + 外来），如下图：

    ```html
    <div class="wrap">
        <div class="wrap-header">
            <div class="wrap-title"></div>
        </div>
        <div class="wrap-body"></div>
    </div>

    <div class="wrap wrap-success">
        <div class="wrap-header"></div>
        <div class="wrap-body"></div>
         <!-- ...  -->
    </div>
    ```
